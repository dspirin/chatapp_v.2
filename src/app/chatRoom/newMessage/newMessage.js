(function() {

    'use strict';

    angular
        .module("app")
        .component("newMessage", {
            templateUrl: 'app/chatRoom/newMessage/newMessage.html',
        	bindings: {
        		user: '<',
        		scroll: '<'
        	},
            controller: ['Chat', function(Chat) {
            	const ctrl = this;

            	ctrl.sendMessage = () => {
		            if(!ctrl.text) {
		                return;
		            }
		            let message = {
		                author: ctrl.user.email,
		                time: firebase.database.ServerValue.TIMESTAMP,
		                content: ctrl.text
		            };
		            Chat.addMessage(message);
		            Chat.scrollDown(ctrl.scroll, 10);
		            playSound();
		            ctrl.text = '';
		        };

		        function playSound() {
		        	let audio = new Audio();
		        	audio.src = 'files/sound.mp3';
		            audio.autoplay = true;
		        }
		    }],
            controllerAs: 'newMessageCtrl'
        });
})();