(function() {

    'use strict';
    
    angular
        .module("app")
        .component("chatRoom", {
            templateUrl: 'app/chatRoom/chatRoom.html',
            controller: ['$interval', 'Auth', 'Chat', 'Data',
            	function ($interval, Auth, Chat, Data) {
			        const ctrl = this;
			        let timer;
			        ctrl.scroll = document.querySelector('.chat');

			        ctrl.$onInit = () => {
			            ctrl.data = Chat.getLastData();
			            ctrl.users = Data;
			            Chat.scrollDown(ctrl.scroll, 1500);
			        };
			        
			        Auth.$onAuthStateChanged(firebaseUser => {
			            if(firebaseUser) {
			                ctrl.user = firebaseUser;
			                timer = $interval(() => {
			                    let index = Math.round(Math.random() * (ctrl.users.length - 1));
			                    Chat.addMessage(ctrl.users[index]);
			                    Chat.scrollDown(ctrl.scroll, 10);    
			                }, 7000);
			            }
			            else {
			                $interval.cancel(timer);
			            }
			        });
			    }
			],
            controllerAs: 'chatRoomCtrl'
        });
})();