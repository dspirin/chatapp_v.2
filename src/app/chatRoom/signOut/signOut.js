(function() {

    'use strict';

    angular
        .module("app")
        .component("signOut", {
            templateUrl: 'app/chatRoom/signOut/signOut.html',
            controller: ['$location', 'Auth', function($location, Auth) {
            	this.signOut = () => {
		            Auth.$signOut();
		            $location.path("/");
		        };
            }],
            controllerAs: 'signOutCtrl'
        });
})();