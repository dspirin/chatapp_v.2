(function() {

    'use strict';

	function Chat ($firebaseArray, $timeout) {
	 	const messagesRef = firebase.database().ref().child("messages");
	 	const factory = {
	 		addMessage: addMessage,
	 		getMessages: getMessages,
	 		getLastData: getLastData,
	 		scrollDown: scrollDown
	 	};
	 	return factory;

	 	function addMessage(message) {
	 		factory.getMessages().$add(message);
	 	}
	 	function getMessages() {
	  		return $firebaseArray(messagesRef);
	 	}
	 	function getLastData() {
	  		return $firebaseArray(messagesRef.limitToLast(30));
	 	}
	 	function scrollDown(el, time) {
            $timeout(() => {
                el.scrollTop = el.scrollHeight;
            }, time)
        }
    }
	   	
	angular
		.module('app')
		.factory('Chat', [
			'$firebaseArray',
			'$timeout',
			Chat]);
})();